﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
	public Vector2Int Pos = new Vector2Int(2,0);
	public Vector2Int PrevPos;
	public Animator Animator;
	//Positioning
	//  0,2  (1,2)  2,2  (3,2)  4,2
	// (0,1) (1,1) (2,1) (3,1) (4,1)
	//  0,0  (1,0)  2,0  (3,0)  4,0
	//()가 안쳐진 곳만 플레이어가 이동 가능.
	//()가 쳐진 곳에는 패턴이 지나갈 수 있음.
	public Transform[] PointsUp = new Transform[5];
	public Transform[] PointsMid = new Transform[5];
	public Transform[] PointsDown = new Transform[5];
	public Transform[,] Points = new Transform[5,3];
	// Use this for initialization
	void Start ()
	{
		Animator = GetComponent<Animator>();
		for (int i = 0; i < 5; i++)
		{
			Points[i,2] = PointsUp[i];
			Points[i,1] = PointsMid[i];
			Points[i,0] = PointsDown[i];
		}
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (transform.position != Points[Pos.x, Pos.y].position && Pos.x > PrevPos.x)
		{
			Animator.speed = 4;
		}
		else if (transform.position != Points[Pos.x, Pos.y].position && Pos.x < PrevPos.x)
		{
			Animator.speed = 0;
		}
		else
		{
			Animator.speed = 1;
		}

		if (Input.GetAxis("Vertical") > 0 && Pos.y != 2)
		{
			PrevPos = Pos;
			Pos.y = 2;
			Animator.SetTrigger("DoJump");
			transform.position = Points[PrevPos.x, PrevPos.y].position;
		}
		if (Input.GetAxis("Vertical") < 0 && Pos.y != 0)
		{
			PrevPos = Pos;
			Pos.y = 0;
			Animator.SetTrigger("DoJump");
			transform.position = Points[PrevPos.x, PrevPos.y].position;
		}

		if (Input.GetAxis("Horizontal") < 0)
		{
			if (Pos.x != 0)
			{
				PrevPos = Pos;
				Pos.x = 0;
				transform.position = Points[PrevPos.x, PrevPos.y].position;
			}
		}
		else if (Input.GetAxis("Horizontal") > 0)
		{
			if (Pos.x != 4)
			{
				PrevPos = Pos;
				Pos.x = 4;
				transform.position = Points[PrevPos.x, PrevPos.y].position;
			}
		}
		else
		{
			if (Pos.x != 2)
			{
				PrevPos = Pos;
				Pos.x = 2;
				transform.position = Points[PrevPos.x, PrevPos.y].position;
			}
		}
		//Debug.Log(Points[Pos.x,Pos.y]);
		Vector3 deltaPos = (Points[Pos.x, Pos.y].position - Points[PrevPos.x, PrevPos.y].position)
		                   * 8f * Time.deltaTime;
		if((Points[Pos.x, Pos.y].position - transform.position).magnitude > deltaPos.magnitude )
			transform.position += deltaPos;
		else
			transform.position = Points[Pos.x, Pos.y].position;

	}
}