﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundLooper : MonoBehaviour
{
	public float BackgroundScrollSpeed = 4;
	public float SkyScrollSpeed = 1;
	public Transform Group1;
	public Transform Group2;
	public Transform Sky1;
	public Transform Sky2;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update ()
	{
		Group1.position += BackgroundScrollSpeed * Time.deltaTime * Vector3.left;
		Group2.position += BackgroundScrollSpeed * Time.deltaTime * Vector3.left;
		if (Group1.position.x < -19)
		{
			Group1.position = new Vector3(Group2.position.x+19,0,0);
		}
		else if (Group2.position.x < -19)
		{
			Group2.position = new Vector3(Group1.position.x+19,0,0);
		}
		Sky1.position += SkyScrollSpeed * Time.deltaTime * Vector3.left;
		Sky2.position += SkyScrollSpeed * Time.deltaTime * Vector3.left;
		if (Sky1.position.x < -19.42f)
		{
			Sky1.position = new Vector3(Sky2.position.x+19.42f,Sky2.position.y,0);
		}
		else if (Sky2.position.x < -19.42f)
		{
			Sky2.position = new Vector3(Sky1.position.x+19.42f,Sky1.position.y,0);
		}
	}
}
